$(document).ready(function () {
  new WOW().init();
  $("#home-slider").owlCarousel({
    loop: true,
    margin: 30,
    dots: true,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      480: {
        items: 1,
      },
      768: {
        items: 3,
      },
      1200: {
        items: 4,
      },
    },
  });
  
});

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}

var ismobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i) != null
var touchorclick = (ismobile)? 'touchstart' : 'click'
var searchcontainer = document.getElementById('searchcontainer')
var searchfield = document.getElementById('search-terms')
var searchlabel = document.getElementById('search-label')
var searchlabel_1 = document.getElementById('search-label-1')

searchlabel.addEventListener(touchorclick, function(e){ // when user clicks on search label
	searchcontainer.classList.toggle('opensearch') // add or remove 'opensearch' to searchcontainer
	if (!searchcontainer.classList.contains('opensearch')){ // if hiding searchcontainer
		searchfield.blur() // blur search field
		e.preventDefault() // prevent default label behavior of focusing on search field again
	}
	e.stopPropagation() // stop event from bubbling upwards
}, false)
searchlabel_1.addEventListener(touchorclick, function(e){ // when user clicks on search label
	searchcontainer.classList.toggle('opensearch') // add or remove 'opensearch' to searchcontainer
	if (!searchcontainer.classList.contains('opensearch')){ // if hiding searchcontainer
		searchfield.blur() // blur search field
		e.preventDefault() // prevent default label behavior of focusing on search field again
	}
	e.stopPropagation() // stop event from bubbling upwards
}, false)
searchfield.addEventListener(touchorclick, function(e){ // when user clicks on search field
	e.stopPropagation() // stop event from bubbling upwards
}, false)

document.addEventListener(touchorclick, function(e){ // when user clicks anywhere in document
	searchcontainer.classList.remove('opensearch')
	searchfield.blur()
}, false)
   $("input[type=number]").keydown(function (e) {
       // Allow: backspace, delete, tab, escape, enter and .
       if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl/cmd+A
           (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: Ctrl/cmd+C
           (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: Ctrl/cmd+X
           (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: home, end, left, right
           (e.keyCode >= 35 && e.keyCode <= 39)) {
           // let it happen, don't do anything
           return;
       }
       // Ensure that it is a number and stop the keypress
       if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
           e.preventDefault();
       }
   });