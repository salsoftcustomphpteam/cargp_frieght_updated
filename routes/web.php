<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('admin/{path}',[App\Http\Controllers\VueController::class, 'admin'])->name('admin')->where('path','.*');
Route::get('user/{path}',[App\Http\Controllers\VueController::class, 'user'])->name('user')->where('path','.*');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
