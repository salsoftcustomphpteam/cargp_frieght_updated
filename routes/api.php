<?php

use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//users
Route::resource('users',UserController::class);

//login
Route::get('getGraphData',[App\Http\Controllers\Api\LoginController::class, 'getGraphData']);

Route::post('login',[App\Http\Controllers\Api\LoginController::class, 'login']);
Route::get('loginUser',[App\Http\Controllers\Api\LoginController::class, 'loginUser']);
Route::get('viewProfile',[App\Http\Controllers\Api\LoginController::class, 'viewProfile']);
Route::get('notifications',[App\Http\Controllers\Api\LoginController::class, 'notifications']);
Route::get('getNotifications',[App\Http\Controllers\Api\LoginController::class, 'getNotifications']);
Route::post('readNotification',[App\Http\Controllers\Api\LoginController::class, 'readNotification']);
Route::post('changePass',[App\Http\Controllers\Api\LoginController::class, 'changePass']);
Route::post('checkPass',[App\Http\Controllers\Api\LoginController::class, 'checkPass']);
Route::put('updateProfile/{id}',[App\Http\Controllers\Api\LoginController::class, 'updateProfile']);
Route::get('forgotPassword',[App\Http\Controllers\Api\LoginController::class, 'forgotPassword']);
Route::get('verifyCode',[App\Http\Controllers\Api\LoginController::class, 'verifyCode']);
Route::post('changePassword',[App\Http\Controllers\Api\LoginController::class, 'changePassword']);
Route::get('getTotalUsers',[App\Http\Controllers\Api\LoginController::class, 'getTotalUsers']);
Route::get('getActivityLogs',[App\Http\Controllers\Api\LoginController::class, 'getActivityLogs']);

//user QoutationsLog
Route::get('getQuoatationDetail',[App\Http\Controllers\Api\QuoatationController::class,'getQuoatationDetail']);
Route::post('addQuatationPrice',[App\Http\Controllers\Api\QuoatationController::class,'addQuatationPrice']);
Route::get('getQuations',[App\Http\Controllers\Api\QuoatationController::class,'index']);

//UserPaymentLog
Route::get('getUserPaymentLog',[App\Http\Controllers\Api\PaymentController::class, 'getUserPaymentLog']);
Route::get('getPaymentDetail',[App\Http\Controllers\Api\PaymentController::class, 'getPaymentDetail']);
Route::post('userAddPackageServiceDetails',[App\Http\Controllers\Api\PaymentController::class, 'userAddPackageServiceDetails']);

//order logs
Route::get('getOrderLogs',[App\Http\Controllers\Api\OrderController::class, 'getOrderLogs']);
Route::get('getUserOrderLogs',[App\Http\Controllers\Api\OrderController::class, 'getUserOrderLogs']);
Route::get('getUserOrderLog',[App\Http\Controllers\Api\OrderController::class, 'getUserOrderLog']);
Route::get('getUserOrderDetails',[App\Http\Controllers\Api\OrderController::class, 'getUserOrderDetails']);
Route::post('changeOrderStatus',[App\Http\Controllers\Api\OrderController::class, 'changeOrderStatus']);

//Feedbacks
Route::get('getFeedbacks',[App\Http\Controllers\Api\FeedbackController::class, 'getFeedbacks']);
Route::get('feedbackDetails',[App\Http\Controllers\Api\FeedbackController::class, 'feedbackDetails']);



Route::group(['prefix'=>'user'],function(){
    Route::post('login',[App\Http\Controllers\Api\user\LoginController::class, 'login']);
    Route::get('loginUser',[App\Http\Controllers\Api\user\LoginController::class, 'loginUser']);
    Route::post('registerCustomer',[App\Http\Controllers\Api\user\LoginController::class, 'registerCustomer']);
    Route::post('registerBusiness',[App\Http\Controllers\Api\user\LoginController::class, 'registerBusiness']);
    Route::get('viewProfile/{id}',[App\Http\Controllers\Api\user\LoginController::class, 'viewProfile']);
    Route::get('editProfile/{id}',[App\Http\Controllers\Api\user\LoginController::class, 'editProfile']);
    Route::post('updateProfile',[App\Http\Controllers\Api\user\LoginController::class, 'updateProfile']);
    Route::post('updatePassword',[App\Http\Controllers\Api\user\LoginController::class, 'updatePassword']);
    Route::get('getUserPaymentLog',[App\Http\Controllers\Api\user\PaymentController::class, 'getUserPaymentLog']);
    Route::get('getQuotations',[App\Http\Controllers\Api\user\QuotationController::class, 'getQuotations']);
    Route::get('getOrdersLog',[App\Http\Controllers\Api\user\OrderController::class, 'getOrdersLog']);
    Route::get('getPackages',[App\Http\Controllers\Api\user\PackageController::class, 'getPackages']);
    Route::get('getQuantity',[App\Http\Controllers\Api\user\PackageController::class, 'getQuantity']);
    Route::get('getQuantityDetails',[App\Http\Controllers\Api\user\PackageController::class, 'getQuantityDetails']);
    Route::post('submitQuoatation',[App\Http\Controllers\Api\user\QuotationController::class, 'submitQuoatation']);
    Route::get('getQuotationDetails/{id}',[App\Http\Controllers\Api\user\QuotationController::class, 'getQuotationDetails']);
    Route::get('changeStatus/{id}',[App\Http\Controllers\Api\user\QuotationController::class, 'changeStatus']);
    Route::post('addQuotationOrder',[App\Http\Controllers\Api\user\QuotationController::class, 'addQuotationOrder']);
    Route::get('getOrderDetails/{id}',[App\Http\Controllers\Api\user\OrderController::class, 'getOrderDetails']);
    Route::get('paymentLogs',[App\Http\Controllers\Api\user\PaymentController::class, 'paymentLogs']);
    Route::post('submitFeddback',[App\Http\Controllers\Api\user\OrderController::class, 'submitFeddback']);
    Route::post('contactUs',[App\Http\Controllers\Api\user\LoginController::class, 'contactUs']);

});