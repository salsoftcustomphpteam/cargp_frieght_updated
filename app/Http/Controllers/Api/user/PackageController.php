<?php

namespace App\Http\Controllers\Api\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Package;
use App\Models\PackageQuantity;
class PackageController extends Controller
{
    public function getPackages(){
        return Package::all();
    }

    public function getQuantity(Request $request){
        $quatity_details = PackageQuantity::where('package_id',$request->package_id)->get();
        return response()->json($quatity_details);
    }

    public function getQuantityDetails(Request $request){
        $quatity_details = PackageQuantity::where('quantity',$request->quantity)->first();
        return response()->json($quatity_details);
    }



  
}
