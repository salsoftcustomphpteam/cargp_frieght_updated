<?php

namespace App\Http\Controllers\Api\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserOrder;
use App\Models\User;
use App\Models\OrderFeedback;
class OrderController extends Controller
{
    public function getOrdersLog(Request $request)
    {
            if($request->search){
                $order_log = UserOrder::where('user_id',$request->id)->where('status','like','%'.$request->search.'%')->paginate($request->entries);
                if($order_log){
                return response()->json($order_log,200);
                }else{
                return response()->json(['message'=>'No Data Found'],403);
                }
             }
            if($request->from && $request->to)
            {
                $from = $request->from;
                $to   = $request->to;
                $order_log = UserOrder::where('user_id',$request->id)->where(function($query) use($from,$to){
                if($from){
                $query->where('created_at','>=',$from);     
                }
                if($to){
                $query->where('created_at','<=',$to);     
                }
                })->paginate($request->entries); 
                return response()->json(['order_log'=>$order_log],200);
            }
         
           else{
                $order_log = UserOrder::where('user_id',$request->id)->paginate($request->entries);
                if($order_log){
                return response()->json($order_log,200);
                }else{
                return response()->json(['message'=>'Something is wrong'],403);
                } 
             }
            
           } 

            public function getOrderDetails($id){
               
                return  UserOrder::with(array('quotation_details','address'))->where('quote_id',$id)->first();
            }

            public function submitFeddback(Request $request){
                //dd($request->all()); 
                $data = array(
                    'order_id' =>$request->orderId,
                    'message' =>$request->message,
                );
                $order_feedback  = OrderFeedback::create($data);
                if($order_feedback){
                return response()->json(['message'=>'Something'],200);
                }else{
                return response()->json(['message'=>'Something is wrong'],403);
                }
            }
}
