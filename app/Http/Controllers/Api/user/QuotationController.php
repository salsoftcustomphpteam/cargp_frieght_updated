<?php

namespace App\Http\Controllers\Api\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserQuoatation;
use App\Models\Address;
use App\Models\UserCardDetails;
use App\Models\QuotationDetails;
use App\Models\PackageQuantity;
use App\Models\UserOrder;
use App\Models\UserPayment;
use Carbon\Carbon;

class QuotationController extends Controller
{
    public function getQuotations(Request $request)
    {
            if($request->from && $request->to)
            {
            $from = $request->from;
            $to   = $request->to;
            $qoutations = UserQuoatation::where('user_id',$request->id)->where(function($query) use($from,$to){
            if($from){
            $query->where('created_at','>=',$from);     
            }
            if($to){
            $query->where('created_at','<=',$to);     
            }
            })->paginate($request->entries); 
            return response()->json(['qoutations'=>$qoutations],200);
            }
            if($request->search){
            $qoutations = UserQuoatation::where('user_id',$request->id)->where('status','like', "%".$request->search."%")->paginate($request->entries);
            return response()->json(['qoutations'=>$qoutations],200);
            }
            else{
            $qoutations  = UserQuoatation::where('user_id',$request->id)->paginate($request->entries);
            return response()->json(['qoutations'=>$qoutations],200);
            }
            
      }

        public function submitQuoatation(Request $request){
         //dd($request->all());
          $qoutation_data  = array(
            'user_id'         =>$request->userId,
            'date'            =>$request->collection_details['collection_date'],  
            'time'            =>$request->collection_details['collection_time_to'],
            'status'          =>'Pending'    
            );
            $quoteId  = UserQuoatation::create($qoutation_data)->id;
            if($quoteId){
              $quotaion_details_data = array(
                'quote_id'         =>$quoteId,
                'quantity_id'      =>$request->quantity, 
                'package_id'       =>$request->package_type,  
                'length_unit'      =>$request->quotation_details['length_unit'],
                'length'           =>$request->quotation_details['length'], 
                'width'            =>$request->quotation_details['width'],    
                'height'           =>$request->quotation_details['height'],  
                'weight_unit'      =>$request->quotation_details['weight_unit'],  
                'weight'           =>$request->quotation_details['weight'], 
                'delivery_note'    =>$request->quotation_details['delivery_note']   
              );

                   QuotationDetails::create($quotaion_details_data);
            }
            if($quoteId){
                $data = array(
                    'collection_address'   => $request->collection_details['address'],
                    'collection_city'      => $request->collection_details['city'],    
                    'collection_region'    => $request->collection_details['region'],
                    'collection_postcode'  => $request->collection_details['post_code'],
                    'collection_country'   => $request->collection_details['country'],
                    'collection_date'      => $request->collection_details['collection_date'],
                    'collection_time_to'   => $request->collection_details['collection_time_to'],
                    'collection_time_from' => $request->collection_details['collection_time_from'],
                    'delivery_address'     => $request->delivery_details['address'],
                    'delivery_city'        => $request->delivery_details['city'],    
                    'delivery_region'      => $request->delivery_details['region'],
                    'delivery_postcode'    => $request->delivery_details['post_code'],
                    'delivery_country'     => $request->delivery_details['country'],
                    'delivery_date'        => $request->delivery_details['delivery_date'],
                    'delivery_time_to'     => $request->delivery_details['delivery_to'],
                    'delivery_time_from'   => $request->delivery_details['delivery_from'],
                    'quote_id'             => $quoteId 
                    );
        
                    $user_address =   Address::create($data);
            }

            return response()->json(['meesage'=>'quotation submit successfully']);
          }

            public function getQuotationDetails($id){
              
            return  UserQuoatation::with(array('quotation_details','address'))->where('id',$id)->first();
            }

            public function changeStatus($id){
              return UserQuoatation::where('id',$id)->update(['status'=>'Cancelled']);
            }

            public function addQuotationOrder(Request $request){

              //dd($request->all());

              $request->validate([ 
                'card_holder_name'    =>'required|min:3|max:7',
                'card_number'         =>'required|digits:16',
                'cvv_code'            =>'required',
                'expiry_date'         =>'required',
                ]);

                $data = array(
                  'user_id'          =>$request->userId,
                  'card_holder_name' =>$request->card_holder_name,
                  'card_number'      =>$request->card_number,
                  'cvv_code'         =>$request->cvv_code,
                  'expiry_date'      =>$request->expiry_date,
                );

                $cardId   = UserCardDetails::create($data)->id;
                if($cardId){
                  $data = array(
                    'card_id'          =>$cardId,
                    'payment_date'     =>date('Y-m-d H:i:s'),
                    'charges'          =>$request->quotePrice,
                    'status'           =>'Paid',
                  );

                  $paymentId   = UserPayment::create($data)->id;
                }
                if($paymentId){
                  $data = array(
                    'user_id'   =>$request->userId,
                    'quote_id'  =>$request->quoteId,
                    'payment_id'=>$paymentId,
                    'date'      =>date('Y-m-d H:i:s'),
                    'time'      =>date('H:i:s'),
                    'status'    =>'Ongoing',                     
                  );

                   $order_details          = UserOrder::create($data);
                   $user_quotation_status  = UserQuoatation::where('id',$request->quoteId)->update(['status'=>'Completed']);
                }

                if($order_details){
                  return response()->json(['meesage'=>'payment successfullly added'],200);
                }else{
                  return response()->json(['meesage'=>'something went wrong'],401);
                }

            }
}
