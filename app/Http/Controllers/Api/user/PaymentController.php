<?php

namespace App\Http\Controllers\Api\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserPayment;
use App\Models\User;
use App\Models\UserOrder;

class PaymentController extends Controller
{
    public function paymentLogs(Request $request){

        $payments_Ids  = UserOrder::where('user_id',$request->id)->pluck('payment_id');
        if($request->from && $request->to)
         {
         $from = $request->from;
         $to   = $request->to;
         $payment_log   = UserPayment::with('order')->whereIn('id',$payments_Ids)->where(function($query) use($from,$to){
         if($from){
         $query->where('created_at','>=',$from);     
         }
         if($to){
         $query->where('created_at','<=',$to);     
         }
          })->paginate($request->entries); 
          return response()->json(['payment_log'=>$payment_log],200);
         }
        if($request->search){    
        $payment_log = UserPayment::with('order')->whereIn('id',$payments_Ids)->where('status','like','%'.$request->search.'%')->paginate($request->entries);
        return response()->json(['payment_log'=>$payment_log],200);        
        }        
        else{
         $payment_log = UserPayment::with('order')->whereIn('id',$payments_Ids)->paginate($request->entries);
         if($payment_log){
         return response()->json(['payment_log'=>$payment_log],200);
         }else{
         return response()->json(['message'=>'Something is wrong'],403);  
        }
     }
      

      }

}
