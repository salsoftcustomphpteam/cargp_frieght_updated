<?php

namespace App\Http\Controllers\Api\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\Notifications;
use App\Mail\ContactUs;
class LoginController extends Controller
{
        public function login(Request $request)
        {
        
            $login_cardtional = array(
                'email'    => $request->email,
                'password' => $request->password,
                'role_type_id'=>2
            );
            if (auth::attempt($login_cardtional)) {
                $token = auth()->user()->createToken('cargo_frieght')->accessToken;
                $data = ['user' => auth()->user()->id, 'token' => $token];
                return response()->json($data, 200);
            }
            else {
                return response()->json(['message' => 'username or password is invalid'], 401);
            }
        }

            public function loginUser(Request $request)
            {
                $user = User::where('id', $request->id)->first();
                if ($user){
                    return response()->json($user,200);
                } else {
                    return response()->json(['message' => 'Somthing is wrong'],403);
                }
            }

            public function viewProfile($id)
            {
                
                $user = User::where('id',$id)->first();
                if($user){
                return response()->json($user,200);
                }else{
                return response()->json(['message'=>'Something is wrong'],403);
                }
            }

        public function editProfile($id)
        {
            $user = User::where('id',$id)->first();
            if($user){
            return response()->json($user,200);
            }else{
            return response()->json(['message'=>'Something is wrong'],403);
            }
        }

        public function updateProfile(Request $request)
        {
            //dd($request->all());
            $request->validate([ 
                'phone_no'    =>'min:11',
                ]); 
                if($request->file() == null){
                $data = array(
                'first_name'  =>$request->first_name,
                'last_name'   =>$request->last_name,
                'email'      =>$request->email,
                'phone_no'    =>$request->phone_no,
                'address'     =>$request->address,
                'city'        =>$request->city,
                'company'     =>$request->company,
                'company_no'  =>$request->company_no,
                'vat_no'      =>$request->vat_no,
                'country'     =>$request->country,
                'post_code'   =>$request->post_code,
                );
                $user  = User::where('id',$request->id)->update($data);
                if($user){
                return response()->json(['message'=>'Profile updated successfylly'],200);
                }else{
                return response()->json(['message'=>'Something is wrong'],403);
                }    
                }
                if($request->profile){
                $imageName = time().'.'.$request->profile->extension();  
                $request->profile->move(public_path('uploads'), $imageName);    
                $data = array(
                'first_name'  =>$request->first_name,
                'last_name'   =>$request->last_name,
                'email'      =>$request->email,
                'phone_no'    =>$request->phone_no,
                'address'     =>$request->address,
                'city'        =>$request->city,
                'country'     =>$request->country,
                'company'     =>$request->company,
                'company_no'  =>$request->company_no,
                'vat_no'      =>$request->vat_no,
                'post_code'   =>$request->post_code,
                'profile'     =>$imageName
                );
                $user  = User::where('id',$request->id)->update($data);
                if($user){
                return response()->json(['message'=>'Profile updated successfylly'],200);
                }else{
                return response()->json(['message'=>'Something is wrong'],403);
                }
             }
        }

        public function updatePassword(Request $request)
        {
            $request->validate([
                'currantPassword' => 'required|min:6',  
                'newPassword'     => 'required|min:6',
                'confirmPassword' => 'required_with:newPassword|same:newPassword|min:6',
            ]);
          $new_password = Hash::make($request->newPassword);
          $update_password = User::where('id',$request->id)->update(['password'=>$new_password]);
          if($update_password){
          return response()->json(['message'=>'Password Update Successfully'],200);
          }else{
            return response()->json(['message'=>'Something is wrong'],403);
          }
        }

        public function registerCustomer(Request $request){
               //dd($request->all());
                $request->validate([
                'first_name'  =>'required|min:3|max:10',
                'last_name'   =>'required|min:3|max:10',
                'email'       =>'required|email',
                'address'     =>'required',
                'password'    =>'required|min:6',
                'confirm_password' =>'same:password|min:6',
                'phone_no'    =>'required|min:11|max:11',
                 ]);

                if($request->profile != 'undefined'){
                    $imageName = time().'.'.$request->profile->extension();  
                    $request->profile->move(public_path('uploads'), $imageName); 
                    $data = array(
                        'reg_date'    =>date('Y-m-d H:i:s'),
                        'user_type'   =>$request->user_type,
                        'first_name'  =>$request->first_name,
                        'last_name'   =>$request->last_name,
                        'email'       =>$request->email,
                        'password'    =>Hash::make($request->password),
                        'phone_no'    =>$request->phone_no,
                        'address'     =>$request->address,
                        'country'     =>$request->country,
                        'city'        =>$request->city,
                        'post_code'   =>$request->post_code,
                        'profile'     =>$imageName,
                        'company_no'  =>$request->company_no,
                        'company'     =>$request->company,
                        'vat_no'      =>$request->vat_no, 
                        );
        
                        $sender_id  = User::create($data)->id;
                        if($sender_id){
                        $adminIds  = User::where('role_type_id',1)->pluck('id');
                        foreach($adminIds as $admins){
                            $data = array(
                                'sender_id'   =>$sender_id,
                                'recipient_id'=>$admins,
                                'message'     =>'New User Account Created Successfully',
                                'date'        =>date('Y-m-d H:i:s'),
                                'url'         =>"/admin/view-profile/$sender_id",
                                'status'      =>0
                            );
                            
                             Notifications::create($data); 
                        } 
                        return response()->json(['message'=>'User created successfylly'],200);
                        }else{
                        return response()->json(['message'=>'Something is wrong'],403);
                        } 
                } else{
                    $data = array(
                        'reg_date'    =>date('Y-m-d H:i:s'),
                        'user_type'   =>$request->user_type,
                        'first_name'  =>$request->first_name,
                        'last_name'   =>$request->last_name,
                        'email'       =>$request->email,
                        'password'    =>Hash::make($request->password),
                        'phone_no'    =>$request->phone_no,
                        'address'     =>$request->address,
                        'country'     =>$request->country,
                        'city'        =>$request->city,
                        'post_code'   =>$request->post_code,
                        'company_no'  =>$request->company_no,
                        'company'     =>$request->company,
                        'vat_no'      =>$request->vat_no, 
                        );
        
                        $sender_id  = User::create($data)->id;
                        if($sender_id){
                        $adminIds  = User::where('role_type_id',1)->pluck('id');
                        foreach($adminIds as $admins){
                            $data = array(
                                'sender_id'   =>$sender_id,
                                'recipient_id'=>$admins,
                                'message'     =>'New User Account Created Successfully',
                                'date'        =>date('Y-m-d H:i:s'),
                                'url'         =>"/admin/view-profile/$sender_id",
                                'status'      =>0
                            );
                            
                             Notifications::create($data); 
                        } 
                        return response()->json(['message'=>'User created successfylly'],200);
                        }else{
                        return response()->json(['message'=>'Something is wrong'],403);
                        } 
                }

            
           }

           public function registerBusiness(Request $request){
              // dd($request->all());
            $request->validate([
                'first_name'  =>'required|min:3|max:10',
                'last_name'   =>'required|min:3|max:10',
                'email'       =>'required|email',
                'address'     =>'required',
                'password'    =>'required|min:6',
                'confirm_password' =>'same:password|min:6',
                'phone_no'    =>'required|min:11|max:11',
                 ]);

                 if($request->profile != 'undefined'){
                    $imageName = time().'.'.$request->profile->extension();  
                    $request->profile->move(public_path('uploads'), $imageName); 
                    $data = array(
                        'reg_date'    =>date('Y-m-d H:i:s'),
                        'user_type'   =>$request->user_type,
                        'first_name'  =>$request->first_name,
                        'last_name'   =>$request->last_name,
                        'email'       =>$request->email,
                        'password'    =>Hash::make($request->password),
                        'phone_no'    =>$request->phone_no,
                        'address'     =>$request->address,
                        'country'     =>$request->country,
                        'city'        =>$request->city,
                        'post_code'   =>$request->post_code,
                        'profile'     =>$imageName,
                        'company_no'  =>$request->company_no,
                        'company'     =>$request->company,
                        'vat_no'      =>$request->vat_no, 
                        );
                        $sender_id  = User::create($data)->id;
                        if($sender_id){
                        $adminIds  = User::where('role_type_id',1)->pluck('id');
                        foreach($adminIds as $admins){
                            $data = array(
                                'sender_id'   =>$sender_id,
                                'recipient_id'=>$admins,
                                'message'     =>'New User Account Created Successfully',
                                'date'        =>date('Y-m-d H:i:s'),
                                'url'         =>"/admin/view-profile/$sender_id",
                                'status'      =>0
                            );
                            
                             Notifications::create($data); 
                        } 
                        return response()->json(['message'=>'User created successfylly'],200);
                        }else{
                        return response()->json(['message'=>'Something is wrong'],403);
                        }  
                } else{
                    $data = array(
                        'reg_date'    =>date('Y-m-d H:i:s'),
                        'user_type'   =>$request->user_type,
                        'first_name'  =>$request->first_name,
                        'last_name'   =>$request->last_name,
                        'email'       =>$request->email,
                        'password'    =>Hash::make($request->password),
                        'phone_no'    =>$request->phone_no,
                        'address'     =>$request->address,
                        'country'     =>$request->country,
                        'city'        =>$request->city,
                        'post_code'   =>$request->post_code,
                        'company_no'  =>$request->company_no,
                        'company'     =>$request->company,
                        'vat_no'      =>$request->vat_no, 
                        );
        
                        $sender_id  = User::create($data)->id;
                        if($sender_id){
                        $adminIds  = User::where('role_type_id',1)->pluck('id');
                        foreach($adminIds as $admins){
                            $data = array(
                                'sender_id'   =>$sender_id,
                                'recipient_id'=>$admins,
                                'message'     =>'New User Account Created Successfully',
                                'date'        =>date('Y-m-d H:i:s'),
                                'url'         =>"/admin/view-profile/$sender_id",
                                'status'      =>0
                            );
                            
                             Notifications::create($data); 
                        } 
                        return response()->json(['message'=>'User created successfylly'],200);
                        }else{
                        return response()->json(['message'=>'Something is wrong'],403);
                        } 
                }
           }

           public function contactUs(Request $request)
           {
            //  //dd($request->all());
            $request->validate([
                'first_name'  =>'required',
                'last_name'   =>'required',
                'phone_number'=>'required',
                'email'       =>'required|email',
                'subject'     =>'required',
                'message'     =>'required'
                 ]);

               $contactus = Mail::to('phpmailer02@gmail.com')->send(new ContactUs($request->all()));
               \Log::channel('mysql')->info('User registered by email without verify');
                return response()->json(['message'=>'Contact Form submit successfully'],200);
               
           }
}
