<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;
use App\Models\ActivityLogs;
use App\Models\Notifications;
use Mockery\Matcher\Not;

class LoginController extends Controller
{
    public function login(Request $request)
    {
       
        $login_cardtional = array(
            'email'    => $request->email,
            'password' => $request->password,
            'role_type_id'=>1
        );
        if (Auth::attempt($login_cardtional)) {
            $token = auth()->user()->createToken('cargo_frieght')->accessToken;
            $data = ['user' => auth()->user()->id, 'token' => $token];
           // dd($token);
            return response()->json($data, 200);
            
        }
        else {
            return response()->json(['message' => 'username or password is invalid'], 401);
        }
    }

    public function loginUser(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        if ($user) {
            return response()->json($user,200);
        } else {
            return response()->json(['message' => 'Somthing is wrong'],403);
        }
    }

    public function viewProfile(Request $request)
    {
        //dd($request->all());
        $user = User::where('id', $request->id)->first();
        //dd($user);
        if ($user) {
            return response()->json($user,200);
        } else {
            return response()->json(['message' => 'Somthing is wrong'],403);
        }
    }

    public function checkPass(Request $request)
    {
        $user      = User::where('id', $request->id)->get('password')->first();
        $user_pass = $user->password;
        //dd(Crypt::decrypt($user_pass));
        $pass_decript = Crypt::decrypt($user_pass);
        // dd($pass_decript);
        if ($request->password == $pass_decript) {
            return response()->json($pass_decript);
        } else {
            return response()->json(['message' => 'Currant password not match'],403);
        }
    }

    public function changePass(Request $request)
    {
        $request->validate([
            'currentPassword' => 'required|min:6',  
            'newPassword'     => 'required|min:6',
            'confirmPassword' => 'required_with:newPassword|same:newPassword|min:6',
        ]);
        $user   = User::where('id',$request->id)->first();
       // dd($user);
      // $current_pass   = Hash::make($request->currentPassword); 
      if(Hash::check($request->currentPassword,$user->password)){
        $new_password = Hash::make($request->newPassword);
        $update_password = User::where('id',$request->id)->update(['password'=>$new_password]);
        return response()->json(['message'=>'Password Update Successfully'],200);
      }else{
        return response()->json(['message'=>'Old Password Not Matched'],422);
      }

        // if(Hash::check($request->currantPassword,$password)) {
        //     $user_updated = User::where('id',$request->id)->update(['password' => Hash::make($request->newPassword)]);
        //     if ($user_updated) {
        //         return response()->json(['message'=>'Password Update Successfully'],200);
        //     }
        //     return response()->json(['message'=>'Error Updating Password'],403);
        // } else {
        //     return response()->json(['message'=>'Something is wrong'],403);
        // }
    }

    public function updateProfile(Request $request)
    {
            //dd($request->all());
            $request->validate([ 
            'phone_no'    =>'min:11',
            ]); 
            if($request->file() == null){
            $data = array(
            'first_name'  =>$request->first_name,
            'last_name'   =>$request->last_name,
            'email'      =>$request->email,
            'phone_no'    =>$request->phone_no,
            'address'     =>$request->address,
            'city'        =>$request->city,
            'country'     =>$request->country,
            'post_code'   =>$request->post_code,
            );
            $user  = User::where('id',$request->id)->update($data);
            if($user){
            return response()->json(['message'=>'Profile updated successfylly'],200);
            }else{
            return response()->json(['message'=>'Something is wrong'],403);
            }    
            }
            if($request->profile){
            $imageName = time().'.'.$request->profile->extension();  
            $request->profile->move(public_path('uploads'), $imageName);    
            $data = array(
            'first_name'  =>$request->first_name,
            'last_name'   =>$request->last_name,
            'email'      =>$request->email,
            'phone_no'    =>$request->phone_no,
            'address'     =>$request->address,
            'city'        =>$request->city,
            'country'     =>$request->country,
            'post_code'   =>$request->post_code,
            'profile'     =>$imageName
            );
            $user  = User::where('id',$request->id)->update($data);
            if($user){
            return response()->json(['message'=>'Profile updated successfylly'],200);
            }else{
            return response()->json(['message'=>'Something is wrong'],403);
            }
         }
    }

        public function forgotPassword(Request $request){
           $user = User::whereEmail($request->email)->first();
           $digits = 4;
           $verificationCode       = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
           if($user){
            User::where('email',$request->email)->update(['verify_code'=>$verificationCode]);   
            Mail::to($request->email)->send(new ForgotPasswordMail($verificationCode));
            return view('forgotpassword.email',['verify_code'=>$verificationCode]);
            return response()->json(['message'=>'Forgot password link sent to your register email'],200);
           }else{
            return response()->json(['message'=>'Sorry! email does not match our record'],403);
           } 
           
        }

        public function verifyCode(Request $request){
            if($request->verify_code == null){
                return response()->json(["message"=>"please enter verification code first"],403);
            }

            $user  = User::where('verify_code',$request->verify_code)->first();
            if($user)
            {
                return response()->json(['verify_code'=>$request->verify_code]);
            }else{
                return response()->json(["message"=>"code is incorrect"],403);
            }
        }

        public function changePassword(Request $request){
          //  dd($request->all());
              $request->validate([
                'password' => 'min:6',
                'password_confirmation' => 'required|same:password|min:6',
                 ]);
                 $newPassword = Hash::make($request->password_confirmation);
                 User::where('email',$request->email)->update(['password' =>$newPassword,'verify_code'=>'']);
                 return response()->json(['message'=>'Password Update Successfully']);
                  
        }

        public function getTotalUsers(){
            $total_users = User::all();
            return response()->json(['total_users'=>$total_users->count()]);
        }

        public function getActivityLogs(){
            $activity_logs = ActivityLogs::all();
            return response()->json(['activity_logs'=>$activity_logs]);
        }
        public function notifications(Request $request){
            //dd($request->all());
            $notifications  = Notifications::where('recipient_id',$request->id)->where('status',0)->take(5)->orderby('id','DESC')->get();
            $count = Notifications::where('recipient_id',$request->id)->where('status',0)->count();
            //dd($admin_notifications);
            return response()->json(['notifications'=>$notifications,'count'=>$count]); 
        }
        public function getNotifications(Request $request){
            $notifications = Notifications::where('recipient_id',$request->id)->orderby('id','DESC')->get();
            return response()->json(['notifications'=>$notifications]);
        }

        public function readNotification(Request $request){
            $notification_id = $request->params['id'];
            $notifications  = Notifications::where('recipient_id',$request->id)->where('status',0)->take(5)->orderby('id','DESC')->get();
            $count = Notifications::where('recipient_id',$request->id)->where('status',0)->count();
            Notifications::where('id',$notification_id)->update(['status'=>1]);
            return response()->json(['notifications'=>$notifications,'count'=>$count]); 
        }

        public function getGraphData(Request $request){
            //dd($request->year);
            $year = request('year', date('Y'));
            //dd($year);
            $from = $year . '-01-01 00:00:00';
            $to   = $year . '-12-31 00:00:00';
    
            $data  = User::selectRaw('MONTH(created_at) as month, COUNT(id) as count')
                ->whereBetween('created_at', [$from, $to])
                ->groupBy('month')
                ->get();

                //dd($data);
            
            $temp = [];
            $totalSum = 0;
            for ($i = 1; $i <= 12; $i++){
                    $row = $data->where('month', $i)->first();
                    $totalSum +=  (int)($row ? $row->count:0);
                    $temp[] = $row? $row->count: 0;
            }
           
            $total_register_users = User::whereBetween('created_at', [$from, $to])->count();

            return response()->json(['graphdata'=>$temp,'total_register_users'=>$total_register_users]);
        }

     
}
