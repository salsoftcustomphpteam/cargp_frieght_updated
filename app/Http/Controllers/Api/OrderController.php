<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserOrder;
use App\Models\User;
use App\Models\UserQuoatation;
use App\Models\PackegeServiceDetails;
use App\Models\PickAndDrop;
use App\Models\UserPayment;

class OrderController extends Controller
{

     public function getUserOrderLogs(Request $request){
           //dd($request->all());
           $user = User::where('id',$request->id)->first();
           
            if($request->from && $request->to){
                $from    = request('from');
                $to      = request('to');
                $entries = request('entries', 10);
                $user_order_log = UserOrder::orderby('id','DESC')->where(function ($query) use($from, $to){
                if($from){
                $query->where('created_at', '>=', $from);
                }
                if($to){
                $query->where('created_at', '<=', $to);
                }        
                })->where('user_id',$request->id)->paginate($entries);
                $data = ['user'=>$user, 'user_order_log'=>$user_order_log];
                return response()->json($data,200);
            }
            if($request->search){
                $user_order_log = UserOrder::orderby('id','DESC')->where('user_id',$request->id)->where('status','like','%'.$request->search.'%')->paginate($request->entries); 
                if($user_order_log){
                $data = ['user'=>$user, 'user_order_log'=>$user_order_log];
                return response()->json($data,200);
                }else{
                return response()->json(['message'=>'Something is wrong'],403);
                }    
               
              }    
                $user_order_log = UserOrder::orderby('id','DESC')->where('user_id',$request->id)->paginate($request->entries); 
                if($user_order_log){
                $data = ['user'=>$user, 'user_order_log'=>$user_order_log];
                return response()->json($data,200);
                }else{
                return response()->json(['message'=>'Something is wrong'],403);
                }
               
       }
    
      public function getUserOrderLog(Request $request){
        if($request->search){
            $order_log = UserOrder::orderby('id','DESC')->where('status','like','%'.$request->search.'%')->paginate($request->entries);
            if($order_log){
            return response()->json($order_log,200);
            }else{
            return response()->json(['message'=>'No Data Found'],403);
            }
        }
            $from   = request('from');
            $to     = request('to');
            $entries = request('entries', 10);
            $order_log = UserOrder::orderby('id','DESC')->where(function ($query) use($from, $to){
            if($from){
            $query->where('created_at', '>=', $from);
            }
            if($to){
            $query->where('created_at', '<=', $to);
            }        
            })->paginate($entries);
             return response()->json($order_log,200);
        if($request->status == 'order-log'){
            $order_log = UserOrder::orderby('id','DESC')->paginate($request->entries);
            if($order_log){
            return response()->json($order_log,200);
            }else{
            return response()->json(['message'=>'Something is wrong'],403);
            }   
        }
        $user  = User::where('id',$request->id)->get()->first();
        $user_order_log = UserOrder::where('user_id',$request->id)->paginate($request->entries);
        if($user){
        $data = ['user'=>$user,'user_order_log'=>$user_order_log];
        return response()->json($data,200);
        }else{
        return response()->json(['message'=>'Something is wrong'],403);
        }
    }

    public function getUserOrderDetails(Request $request){
        $user_order_log = UserOrder::with('user','quotation_details','address')->where('id',$request->id)->first();
        if($user_order_log){
        return response()->json($user_order_log,200);    
        }else{
        return response()->json(['message'=>'Something is wrong'],403);    
        }
    }

        public function changeOrderStatus(Request $request){
             $order_status  = UserOrder::where('id',$request->id)->update(['status'=>$request->status]);
             if($order_status){
                return response()->json(['message'=>'Order Status Updated Successfully'],200);    
                }else{
                return response()->json(['message'=>'Something is wrong'],403);    
                }
        }

}
