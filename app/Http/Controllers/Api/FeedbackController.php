<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OrderFeedback;
use App\Models\User;
use App\Models\UserOrder;

class FeedbackController extends Controller
{
    public function getFeedbacks(Request $request){
         $userIds   = UserOrder::pluck('user_id');
        if($request->search) {
            $entries = request('entries', 10);
            $feedbacks = User::whereIn('id',$userIds)->where('first_name', 'like', "%" .$request->search. "%")->paginate($entries);
            return response()->json($feedbacks);
           } 
        if($request->from && $request->to){

            $from   = request('from');
            $to     = request('to');
            $entries = request('entries', 10);
            $feedbacks = User::whereIn('id',$userIds)->orderby('id','DESC')->where(function ($query) use($from, $to){
            if($from){
            $query->where('created_at', '>=', $from);
            }
            if($to){
            $query->where('created_at', '<=', $to);
            }        
            })->paginate($entries);
          }   
            $feedbacks  = User::whereIn('id',$userIds)->paginate($request->entries); 
            if($feedbacks){
            return response()->json($feedbacks,200);
            }else{
            return response()->json(['message'=>'Something is wrong'],403);
         }

    } 

        public function feedbackDetails(Request $request){
            $feedback_details  = UserOrder::with('orderfeedback')->where('user_id',$request->id)->first();
            return $feedback_details;
        }

}