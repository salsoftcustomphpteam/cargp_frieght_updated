<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserQuoatation;
use App\Models\PackegeServiceDetails;
use App\Models\PickAndDrop;
use App\Models\UserPayment;
class QuoatationController extends Controller
{
    public function index(Request $request){

        if($request->search){
        $qoutations = UserQuoatation::orderby('id','DESC')->where('status','like', "%".$request->search."%")->paginate($request->entries);
        return response()->json(['qoutations'=>$qoutations],200);
        }
        else{
        $qoutations  = UserQuoatation::orderby('id','DESC')->paginate($request->entries);
        return response()->json(['qoutations'=>$qoutations],200);
        }
        $from   = request('from');
        $to     = request('to');
        $entries = request('entries', 10);
        $qoutations = UserQuoatation::orderby('id','DESC')->where(function ($query) use($from, $to){
        if($from){
        $query->where('created_at', '>=', $from);
        }
        if($to){
        $query->where('created_at', '<=', $to);
        }        
        })->paginate($entries);
        //dd($users);
        if($qoutations){
        return response()->json(['qoutations'=>$qoutations],200);
        }else{
        return response()->json(['message'=>'Something is wrong'],403);
        }
    }

    public function getQuoatationDetail(Request $request){
        $qoutation = UserQuoatation::with('user','quotation_details','address')->where('id',$request->id)->first();
        if($qoutation){   
        return response()->json($qoutation,200);    
        }else{
        return response()->json(['message'=>'Something is wrong'],403);    
        }   

    }

    public function getQuations(Request $request){
     
        $quatations  = UserQuoatation::all();
        if($quatations){
        return response()->json($quatations,200);    
        }
        else{
        return response()->json(['message'=>'Something is wrong'],403);    
        }
    }

        public function addQuatationPrice(Request $request){
             $user_quotation = UserQuoatation::where('id',$request->id)->update(['quotation_price'=>$request->quatation_price,'status'=>'Received']);
             return response()->json(['message'=>'Quotation Updated Successfully!'],200);  
        }
}
