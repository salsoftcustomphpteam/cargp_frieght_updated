<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserPayment;
use App\Models\PackegeServiceDetails;
use App\Models\PickAndDrop;
use App\Models\UserOrder;

class PaymentController extends Controller
{       
                public function getUserPaymentLog(Request $request){     

                $user = User::where('id',$request->id)->first();       
                $payment_Ids      = UserOrder::where('user_id',$request->id)->pluck('payment_id');

                if($request->from && $request->to){
                        //dd('yes');
                        $from    = request('from');
                        $to      = request('to');
                        $entries = request('entries', 10);
                        $payment_log = UserPayment::orderby('id','DESC')->where(function ($query) use($from, $to){
                        if($from){
                        $query->where('created_at', '>=', $from);
                        }
                        if($to){
                        $query->where('created_at', '<=', $to);
                        }        
                        })->paginate($entries);
                       // dd($payment_log);
                        return response()->json(['payment_log'=>$payment_log],200);
                }        

                if($request->search){  
                $user_payment_log = UserPayment::with('order')->where('status','like','%'.$request->search.'%')->whereIn('id',$payment_Ids)->paginate($request->entries);
                if($user_payment_log){
                $data = ['user'=>$user, 'user_payment_log'=>$user_payment_log];
                return response()->json($data,200);
                }else{
                return response()->json(['message'=>'Something is wrong'],403);
                }        
                }        
                if($request->status == 'payment-log'){
                $payment_log = UserPayment::with('order')->paginate($request->entries);
                if($payment_log){
               // $data = ['user_payment_log'=>$user_payment_log];
                return response()->json(['payment_log'=>$payment_log],200);
                }else{
                return response()->json(['message'=>'Something is wrong'],403);
                }   

                }

                $user_payment_log = UserPayment::with('order')->whereIn('id',$payment_Ids)->paginate($request->entries);
                if($user){
                $data = ['user'=>$user,'user_payment_log'=>$user_payment_log];
                return response()->json($data,200);
                }else{
                return response()->json(['message'=>'Something is wrong'],403);
                }
        
                }

        public function getPaymentDetail(Request $request){
        
                $payment = UserPayment::where('id',$request->id)->first();
                //dd($qoutation);
                $user = User::where('id',$payment->user_id)->where('role_type_id','!=',1)->get()->first();
                $packege_service_details= PackegeServiceDetails::where('payment_id',$payment->id)->first();
                //dd($packege_service_details);
                $pick_drop_details= PickAndDrop::where('payment_id',$payment->id)->first();
                //dd($pick_drop_details);
                if($payment){
                $data = ['user'=>$user,'payment'=>$payment,'packege_service_details'=>$packege_service_details,'pick_drop_details'=>$pick_drop_details];     
                return response()->json($data,200);    
                }else{
                return response()->json(['message'=>'Something is wrong'],403);    
                }   
        
            }
        
        public function userAddPackageServiceDetails(Request $request){
               //dd($request->all());
               $packege_service_details = PackegeServiceDetails::find($request->id);
               if($packege_service_details){
                $data = array( 
                        "quotation_price" => $request->quatation_price,   
                        "is_loading"      => $request->is_loading, 
                        "is_unloading"    => $request->is_unloading, 
                     );     
                 $result =   PackegeServiceDetails::where('id',$request->id)->update($data);
                 if($result){
                 return response()->json(['message'=>'Service Details Updated Now'],);
                 }else{
                 return response()->json(['message'=>'Something is wrong'],403); 
                 }

               }
               
        }    
}
