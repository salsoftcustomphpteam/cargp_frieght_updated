<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserQuoatation extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function address(){
        return $this->belongsTo(Address::class,'id','quote_id');
    }

    public function quotation_details(){
        return $this->belongsTo(QuotationDetails::class,'id','quote_id');
    }

}
