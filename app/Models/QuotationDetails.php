<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationDetails extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $with    = ['quantity','package'];

    public function quantity(){
        return $this->belongsTo(PackageQuantity::class,'quantity_id','id');
    }

    public function package(){
        return $this->belongsTo(Package::class,'package_id','id');
    }

}
