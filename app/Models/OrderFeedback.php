<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderFeedback extends Model
{
    use HasFactory;
    protected $guarded = [];
   // protected $with    = ['order'];
    public function order(){
        return $this->belongsTo(UserOrder::class,'order_id','id');
    }
}
