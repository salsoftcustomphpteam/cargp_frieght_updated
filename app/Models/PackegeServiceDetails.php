<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackegeServiceDetails extends Model
{
    use HasFactory;
    
    protected $fillable = [

        'payment_id',
        'package_type',
        'service_type',
        'collection_time',
        'quantity',
        'length',
        'width',
        'height',
        'weight_unit',
        'quotation_price',
        'quote_id',
        'is_loading',
        'is_unloading'
    ];
}
