<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageQuantity extends Model
{
    use HasFactory; 
    // protected $with = ['packageone'];

    public function packageone(){
        return $this->hasMany(Package::class,'id','package_id');
    }

}
