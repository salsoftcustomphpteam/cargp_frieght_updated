<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $with = ['package_quantity'];
    
    public function package_quantity(){
        return $this->hasMany(PackageQuantity::class);
    }

}
