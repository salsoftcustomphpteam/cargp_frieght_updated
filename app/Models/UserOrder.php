<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOrder extends Model
{
    use HasFactory;
    protected $guarded = [];
   // protected $with = ['user'];
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function orderfeedback(){
        return $this->belongsTo(OrderFeedback::class,'id','order_id');
    }

    public function address(){
        return $this->belongsTo(Address::class,'quote_id','quote_id');
    }

    public function quotation_details(){
        return $this->belongsTo(QuotationDetails::class,'quote_id','quote_id');
    }
}
