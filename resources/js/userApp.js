import Vue from 'vue';
import router from './components/pages/user/router'; 
import DateFormet from 'moment';
import moment from 'moment';
import VuePagination from 'laravel-vue-pagination';
import Datepicker from 'vue2-datepicker';
import ImageUploader from 'vue-image-upload-resize';
import VueToast from 'vue-toasted';
import App from './components/App.vue';
import { ValidationProvider, extend, ValidationObserver } from "vee-validate";
import {required,email,image} from "vee-validate/dist/rules";
import VueCharts from 'vue-chartjs'
import VueFormWizard from 'vue-form-wizard';

extend ('required', {
    ...required,
    message: 'This field is required'
});
extend ('email', {
    ...email,
    message: 'Please enter valid email address'
});
extend ('image', {
    ...image,
    message: 'File type must be jpg jpeg png'
});

Vue.use(VuePagination)
Vue.use(ImageUploader)
Vue.use(VueToast)
Vue.use(DateFormet)
Vue.use(VueCharts)
Vue.use(VueFormWizard)

Vue.component("moment",moment);
Vue.component("ValidationObserver", ValidationObserver);
Vue.component("ValidationProvider", ValidationProvider);
Vue.component("VueCharts", VueCharts);
Vue.prototype.moment = moment
Vue.use(DateFormet)



Vue.component('ValidationProvider', ValidationProvider);
Vue.component('VueToast', VueToast);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('pagination', require('laravel-vue-pagination'));
require('./bootstrap');
//require('./components/plugins/vee-validate');
window.Vue = require('vue').default;

window.axios.defaults.baseURL = base_url + '/api/';

new Vue({
    router:router,
    render : h => h(App),
}).$mount('#app');