import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from './Login';
import Logout from './Logout';
import Index from './Index';
import AboutUs from './AboutUs.vue';
import AddCustomerResidential from './AddCustomerResidential';
import AddBussniss from './AddBussniss';
import Services from './Services';
import GetQuotation from './GetQuotation';
import QuotationsLog from './QuotationLog.vue';
import OrderLog from './OrderLog';
import PaymentLog from './PaymentLog';
import OurTeam from './OurTeam';
import ViewProfile from './ViewProfile';
import EditUser from './EditUser';
import ContactUs from './ContactUs';
import UserQuotationLog from './UserQuotationLog';
import QuotationDetails from './QuotationDetails';
import Profile from './Profile'
import EditProfile from './EditProfile'
import UserPaymentLog from './UserPaymentLog'
import UserPaymentLogDetails from './UserPaymentLogDetails'
import UserOrderLog from './UserOrderLog'
import OrderLogDetails from './OrderLogDetails'
import ForgotPassword from './ForgotPassword'
import NewPassword from './NewPassword'
import Notifications from './ViewNotifications'


Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base : process.env.NODE_ENV == 'development' ? '/cargo_frieght':'/cargo_frieght',
    routes:[
        {
            path:'/user/home',
            name:'home',
            component:Index,
            meta:{requireAuth:false}
        },
        {
            path:'/user/login',
            name:'login',
            component:Login,
              meta:{requireAuth:false}
        },
        {
            path:'/user/register-customer-residential',
            name:'customer-residential',
            component:AddCustomerResidential,
              meta:{requireAuth:false}
        },
        {
            path:'/user/register-business',
            name:'business',
            component:AddBussniss,
               meta:{requireAuth:false}
        },
        {
            path:'/user/about-us',
            name:'about-us',
            component:AboutUs,
            meta:{requireAuth:false}
        },
        {
            path:'/user/services',
            name:'services',
            component:Services,
             meta:{requireAuth:false}
        },
        {
            path:'/user/our-team',
            name:'our-team',
            component:OurTeam,
              meta:{requireAuth:false}
        },
        {
            path:'/user/contact-us',
            name:'contact-us',
            component:ContactUs,
              meta:{requireAuth:false}
        },
        {
            path:'/user/get-quotation',
            name:'get-quotation',
            component:GetQuotation,
            meta:{requireAuth:true}
        },
        {
            path:'/user/quotation-detail/:id',
            name:'quotation-detail',
            component:QuotationDetails,
              meta:{requireAuth:true}
        },
        {
            path:'/user/payment-log',
            name:'payment-log',
            component:PaymentLog,
              meta:{requireAuth:true}
        },
        {
            path:'/user/quotation-log',
            name:'quotation-log',
            component:QuotationsLog,
              meta:{requireAuth:true}
        },

        {
            path:'/user/edit-profile/:id',
            name:'edit-profile',
            component:EditProfile,
              meta:{requireAuth:true}
        },

        {
            path:'/user/order-log',
            name:'order-log',
            component:OrderLog,
              meta:{requireAuth:true}
        },
        
        {
            path:'/user/order-details/:id',
            name:'order-details',
            component:OrderLogDetails,
              meta:{requireAuth:true}
        },
     
        {
            path:'/user/view-profile/:id',
            name:'view-profile',
            component:ViewProfile,
              meta:{requireAuth:true}
        },


        {
            path:'/logout',
            name:'logout',
            component:Logout,
              meta:{requireAuth:true}
        },
    ]
  });

router.beforeEach((to, from, next) => {
 if(to.meta.requireAuth){
 const  userType       = localStorage.getItem('userType');
 const accessToken =  localStorage.getItem('access-token');
 if(userType == 'user' && accessToken){
        next();
        }else{
        next({path:'/user/login'});
        }
        }
       next();
})


export default router;