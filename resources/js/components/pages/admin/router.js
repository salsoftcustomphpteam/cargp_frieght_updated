import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from './Login';
import Logout from './Logout';
import Index from './Index';
import Users from './Users';
import AddCustomerResidential from './AddCustomerResidential';
import AddBussniss from './AddBussniss';
import BlockedUserListing from './BlockedUserListing';
import QuotationLog from './QuotationLog';
import OrderLog from './OrderLog';
import PaymentLog from './PaymentLog';
import Feedback from './Feedback';
import FeedbackDetails from './FeedbackDetails';
import ViewProfile from './ViewProfile';
import EditUser from './EditUser';
import BlockUsers from './BlockUsers';
import UserQuotationLog from './UserQuotationLog';
import UserQuotationDetails from './UserQuotationDetails';
import Profile from './Profile';
import EditProfile from './EditProfile';
import UserPaymentLog from './UserPaymentLog';
import UserPaymentLogDetails from './UserPaymentLogDetails';
import UserOrderLog from './UserOrderLog';
import UserOrderLogDetails from './UserOrderLogDetails';
import ForgotPassword from './ForgotPassword';
import NewPassword from './NewPassword';
import Notifications from './ViewNotifications';


Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    base : process.env.NODE_ENV == 'development' ? '/cargo_frieght':'/cargo_frieght',
    routes:[
        {
            path:'/admin/login',
            name:'login',
            component:Login,
            meta:{requireAuth:false}
        },
        {
            path:'/forgot-password',
            name:'forgot-password',
            component:ForgotPassword,
            meta:{requireAuth:false}
        },
        {
            path:'/new-password',
            name:'new-password',
            component:NewPassword,
             meta:{requireAuth:false}
        },
        {
            path:'/admin/logout',
            name:'logout',
            component:Logout,
             meta:{requireAuth:true}
        },
        {
            path:'/admin/profile',
            name:'profile',
            component:Profile,
             meta:{requireAuth:true}
        },
        {
            path:'/admin/notifications',
            name:'notifications',
            component:Notifications,
             meta:{requireAuth:true}
        },
        {
            path:'/admin/edit-profile/:id',
            name:'edit-profile',
            component:EditProfile,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/dashboard',
            name:'dashboard',
            component:Index,
            meta:{requireAuth:true}
        },
        {
            path:'/admin/users',
            name:'users',
            component:Users,
            meta:{requireAuth:true}
        },
        {
            path:'/admin/add-customer-residential',
            name:'add-customer-residential',
            component:AddCustomerResidential,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/add-bussniss',
            name:'add-bussniss',
            component:AddBussniss,
              meta:{requireAuth:true}
        },
       
        {
            path:'/admin/quotation-log',
            name:'quotation-log',
            component:QuotationLog,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/order-log',
            name:'order-log',
            component:OrderLog,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/payment-log',
            name:'payment-log',
            component:PaymentLog,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/feedback',
            name:'feedbacks',
            component:Feedback,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/feedback-details/:id',
            name:'feedback-details',
            component:FeedbackDetails,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/view-profile/:id',
            name:'view-profile',
            component:ViewProfile,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/edit-user/:id',
            name:'edit-user',
            component:EditUser,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/block-users',
            name:'block-users',
            component:BlockUsers,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/user-quotation-log/:id',
            name:'user-qoutation-log',
            component:UserQuotationLog,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/user-quotation-detail/:id',
            name:'user-qoutation-detail',
            component:UserQuotationDetails,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/user-payment-log/:id',
            name:'user-payment-log',
            component:UserPaymentLog,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/user-payment-log-details/:id',
            name:'user-payment-log-details',
            component:UserPaymentLogDetails,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/user-order-log/:id',
            name:'user-order-log',
            component:UserOrderLog,
              meta:{requireAuth:true}
        },
        {
            path:'/admin/user-order-log-details/:id',
            name:'user-order-log-details',
            component:UserOrderLogDetails,
              meta:{requireAuth:true}
        },
    ]
  });


 router.beforeEach((to, from, next) => {
 if(to.meta.requireAuth){
 const  userType       = localStorage.getItem('userType');
 const accessToken =  localStorage.getItem('access-token');
 if(userType == 'admin' && accessToken){
        next();
        }else{
        next({path:'/admin/login'});
        }
        
        }
       next();
})


export default router;