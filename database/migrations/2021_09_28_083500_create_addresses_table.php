<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('quote_id');
            $table->string('collection_address');
            $table->string('collection_city');
            $table->string('collection_region');
            $table->string('collection_postcode');
            $table->string('collection_country');
            $table->string('collection_date');
            $table->string('collection_time_to');
            $table->string('collection_time_from');
            $table->string('delivery_address');
            $table->string('delivery_city');
            $table->string('delivery_region');
            $table->string('delivery_postcode');
            $table->string('delivery_country');
            $table->string('delivery_date');
            $table->string('delivery_time_to');
            $table->string('delivery_time_from');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
